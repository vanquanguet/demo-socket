package chat;

import akka.NotUsed;
import akka.japi.Pair;
import akka.stream.Materializer;
import akka.stream.javadsl.*;
import play.engineio.EngineIOController;
import play.socketio.javadsl.SocketIO;
import play.socketio.javadsl.SocketIOEventCodec;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class ChatEngine implements Provider<EngineIOController> {

  private final EngineIOController controller;

  @Inject
  public ChatEngine(SocketIO socketIO, Materializer materializer) {

    //codec
    SocketIOEventCodec<String, String> codec = new SocketIOEventCodec<String, String>() {
      {
        addDecoder("chat message", decodeJson(String.class));
        addEncoder("chat message", String.class, encodeJson());
      }
    };

//    Pair<Sink<String, NotUsed>, Source<String, NotUsed>> pair = MergeHub.of(String.class)
//            .toMat(BroadcastHub.of(String.class), Keep.both()).run(materializer);
//
//    Flow<String, String, NotUsed> chatFlow = Flow.fromSinkAndSourceCoupled(pair.first(), pair.second());

//    controller = socketIO.createBuilder()
//            .addNamespace(codec, (session, namespace) -> {
//              if (namespace.equals("/chat")) {
//                return Optional.of(Flow.<String>create().map(message -> {
//                          return "Success - " + "Your message is " + message;
//                        }
//                ));
//              } else {
//                return Optional.empty();
//              }
//            })
//            .createController();
      controller = socketIO.createBuilder()
              .addNamespace("/chat", codec, Flow.<String>create().map(message -> {
                  System.out.println("ok");
                  return "Success - " + "Your message is " + message;
              })).createController();
  }
  public EngineIOController get() {
    return controller;
  }
}
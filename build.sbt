name := "socket-chat"
 
version := "1.0" 
      
lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

//resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
libraryDependencies += guice
scalaVersion := "2.12.4"
crossScalaVersions := Seq("2.11.12", "2.12.4")

libraryDependencies += "com.lightbend.play" %% "play-socket-io" % "1.0.0-beta-2"
libraryDependencies +=  "org.projectlombok" % "lombok" % "1.16.16"
libraryDependencies += "javax.el" % "javax.el-api" % "3.0.0"
libraryDependencies += "org.glassfish.web" % "javax.el" % "2.2.6"
libraryDependencies += "javax.validation" % "validation-api" % "2.0.0.Final"
libraryDependencies += "org.mindrot" % "jbcrypt" % "0.3m"
libraryDependencies += "commons-codec" % "commons-codec" % "1.10"

routesGenerator := InjectedRoutesGenerator


// Test Database
libraryDependencies += "com.h2database" % "h2" % "1.4.196"
// Testing libraries for dealing with CompletionStage...
libraryDependencies += "org.assertj" % "assertj-core" % "3.6.2" % Test
libraryDependencies += "org.awaitility" % "awaitility" % "2.0.0" % Test
// Make verbose tests
testOptions in Test := Seq(Tests.Argument(TestFrameworks.JUnit, "-a", "-v"))

      